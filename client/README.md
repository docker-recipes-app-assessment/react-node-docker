
## Build docker image

docker build -t recipesapi .

## start docker container

docker run -d -p 3000:3001 --name recipesapi recipesapi .