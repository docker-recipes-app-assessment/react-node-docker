import React, { useGlobal, getGlobal }  from "reactn";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import PrivateRoute from './utils/PrivateRoute'

import Login from './pages/Login';
import Home from './pages/Home';
import AddRecipe from './pages/AddRecipe';
import EditRecipe from './pages/EditRecipe';
import ViewRecipe from './pages/ViewRecipe';

import Header from './components/header.js';

export default function BasicExample() {
  // const userIsLoggedIn = ;
  return (
    <Router>      
      <section id="App">
        <Switch>        
              <Route exact path="/login" component={Login} />         
              <PrivateRoute exact path="/" component={Home} />
              <PrivateRoute path="/add" component={AddRecipe} />
              <PrivateRoute path="/edit/:id" component={EditRecipe} />
              <PrivateRoute path="/view/:id" component={ViewRecipe} />
        </Switch>
      </section>
    </Router>
  );
}

