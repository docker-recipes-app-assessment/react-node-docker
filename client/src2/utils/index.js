import config from "../app.config";
import DemoData from './sample/demodata.json';

let methods = {};

methods.login = async (user) => {
  let apiEndpoint = methods.apiEndpoint('authentication');
  let config = {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(user)
  }
  let user_ = await fetch(apiEndpoint, config).then(res => {
    return res.json()
  }).then(res => {
    // console.log('[[ Authenticate ]]', res)
    return res
  })
  return user_
}
methods.register = async (user) => {
  let apiEndpoint = methods.apiEndpoint('users');
  let config = {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(user)
  }
  let user_ = await fetch(apiEndpoint, config).then(res => {
    return res.json()
  }).then(res => {
    // console.log('[[ Register User ]]', res)
    return res
  })
  return user_
}
methods.apiEndpoint = (collectionName) => {
  const env = process.env.NODE_ENV;
  let api = "";
  if (env === "development") {
    api = config.api.dev;
  }
  else {
    api = config.api.prod;
  }
  return api + "/" + collectionName;
}
methods.fetchRecipes = async () => {
  // const { cuisine } = config;
  let apiEndpoint = methods.apiEndpoint('recipes');
  let config = {
    method: "GET",
    headers: {
      "content-type": "application/json"
    }
  }
  let recipes = await fetch(apiEndpoint, config).then(res => {
    return res.json()
  }).then(res => {
    console.log('[[ Fetch Recipes ]]', res)
    return res.data
  })
  return recipes
}
methods.fetchRecipesByCuisine = async (config_) => {
  const { cuisine } = config_;
  let apiEndpoint = methods.apiEndpoint('recipes') + "/?" + "cuisine=" + cuisine;
  let config = {
    method: "GET",
    headers: {
      "content-type": "application/json"
    }
  }
  let recipes = await fetch(apiEndpoint, config).then(res => {
    return res.json()
  }).then(res => {
    console.log('[[ Fetch Recipes ]]', res)
    return res.data
  })
  return recipes
}
methods.fetchRecipe = async (id) => {
  let apiEndpoint = methods.apiEndpoint('recipes');
  let config = {
    method: "GET",
    headers: {
      "content-type": "application/json"
    }
  }
  let recipe = await fetch(apiEndpoint + "/" + id, config).then(res => {
    return res.json()
  }).then(res => {
    console.log('[[ Fetch Single Recipe ]]', res)
    return res
  })
  return recipe
}
methods.insertRecipe = async (data) => {
  let apiEndpoint = methods.apiEndpoint('recipes');
  let config = {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(data)
  }
  let status;
  let recipe = await fetch(apiEndpoint, config).then(res => {
    status = res.ok;
    return res.json()
  }).then(res => {
    console.log('[[ Insert Recipe ]]', res)
    return res
  })
  return { recipe, status }
}
methods.updateRecipe = async (data_) => {
  let { data, id } = data_;
  let apiEndpoint = methods.apiEndpoint('recipes');
  let config = {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(data)
  }
  let status;
  let recipe = await fetch(apiEndpoint + "/" + id, config).then(res => {
    status = res.ok;
    return res.json()
  }).then(res => {
    console.log('[[ Update Recipe ]]', res)
    return res
  })
  return { recipe, status }
}
methods.deleteRecipe = async (id) => {
  let apiEndpoint = methods.apiEndpoint('recipes');
  let config = {
    method: "DELETE",
    headers: {
      "content-type": "application/json"
    }
  }
  let status;
  let recipe = await fetch(apiEndpoint + "/" + id, config).then(res => {
    status = res.ok;
    console.log(res);
    return res.json()
  }).then(res => {
    console.log('[[ Delete Recipe ]]', res)
    return res
  })
  return { recipe, status }
}
methods.initialiseEditForm = async (config) => {
  const { self } = config;
  let recipeData = JSON.parse(localStorage.getItem('recipeData'))
  self.setState({
    editing: true
  })
  const formConfigFields = self.state.formConfig.fields;
  let fields = [];
  let form = {};
  for (var key in recipeData) {
    if (recipeData.hasOwnProperty(key)) {
      for (let item of formConfigFields) {
        if (item.title === key) {
          let field = { title: key, label: item.label, type: item.type, defaultValue: recipeData[key], value: recipeData[key] }
          fields.push(field)
          form[key] = field;
        }
      }
    }
  }
  let formConfig = {
    fields: fields
  };
  self.setState({ formConfig: formConfig, form: form, ready: true })
}
methods.initialiseForm = async (config_) => {
  const { self } = config_;

  const formConfig = JSON.parse(window.localStorage.getItem('formConfig'));
  const fields = formConfig.fields;
  console.log('initialiseForm', formConfig)
  let form = {};
  for (let item of fields) {
    const title = item.title;
    const value = item.value;
    // let data = { title, value };
    form[item.title] = { title: title, value: value };
  }
  
  self.setState({ form: form, formConfig: formConfig })
  setTimeout(() => { self.setState({ ready: true }); console.log(self.state) }, 300)
}
methods.importDemoData = async () => {

  let length = DemoData.length;
  console.log('[[ DemoData ]]', DemoData, length);
  let res = [];
  for (let item of DemoData) {
    let res_ = await methods.insertRecipe(item)
    res.push(res_)
  }
  return { res, length }

}
methods.initialiseSearchTags = async (recipes) => {
  // console.log('initialiseSearchTags recipes',recipes)
  let tags = [];
  for (let item of recipes) {
    let ingredients = item.ingredientsItems;
    // console.log('ingredients', ingredients)
    for (let item of ingredients) {
      if (!tags.includes(item))
        tags.push(item)
    }
  }
  return tags
}
methods.fetchRecipesByIngredients = async (config) => {
  const { tags, recipes } = config;
  let res = [];
  for (let recipe of recipes) {
    for (let tag of tags) {
      if (recipe.ingredientsItems.includes(tag)) {
        if (!res.includes(recipe)) {
          res.push(recipe)
        }
      }
    }
  }
  return res
}
export default methods;

