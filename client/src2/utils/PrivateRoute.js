import React, { getGlobal } from "reactn";
import PropTypes from "prop-types";
import {
    BrowserRouter as 
    Route   
  } from "react-router-dom";
function goLogin() {
 return window.location.href = "/login"
}
const PrivateRoute = ({ component: Component, path, ...rest }) => {

    const isAuthenticated = getGlobal().userIsLoggedIn;
    const render = props =>
        isAuthenticated === true ? <Component {...props} /> : goLogin();

    return <Route path={path} render={render} {...rest} />;
};

PrivateRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
        .isRequired,
    path: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string)
    ]).isRequired
};

export default PrivateRoute;
