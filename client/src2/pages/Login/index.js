import React, { setGlobal, getGlobal } from 'reactn';
import {
    BrowserRouter as     
    Redirect
} from "react-router-dom";
import { Icon, notification } from 'antd';

import methods from '../../utils';
import Wrapper from '../../components/Wrapper';
import PageTitle from '../../components/PageTitle';

class Login extends React.Component {
    constructor() {
        super()
        this.state = {
            register: false,
            buttonLoading: false,
            redirectHome: false
        }
        this.login = this.login.bind(this)
        this.register = this.register.bind(this)
        this.switchFormType = this.switchFormType.bind(this)
        this.openNotification = this.openNotification.bind(this)

        this.email = React.createRef();
        this.password = React.createRef()
    }

    openNotification = (data) => {
        const { title, message } = data;

        notification.open({
            message: title,
            description:
                message,
            onClick: () => {
            }
        })
        
    }
    async login(e) {
        e.preventDefault()
        this.setState({ buttonLoading: true })
        let user = {
            email: this.email.current.value,
            password: this.password.current.value,
            strategy: "local"
        }
        console.log('[[ User Login ]]', user)
        let res = await methods.login(user)
        console.log('[[ User Login Response ]]',res)
        if(res.code === 401) {
            setTimeout(() => {
                this.openNotification({type: "login", title: "Error", message: "Invalid password or email not found. Please try again or register."})
                setGlobal({
                    userIsLoggedIn: false
                })
                let g = getGlobal();
                localStorage.setItem('g', JSON.stringify(g));
                this.setState({
                    buttonLoading: false,
                    redirectHome: false
                })
            },1000) 
        }
        else if(res.accessToken !== "undefined"){
            setTimeout(() => {
                this.openNotification({type: "login", title: "Success", message: "Logged In"})
                setGlobal({
                    accessToken: res.accessToken,
                    userIsLoggedIn: true
                })
                let g = getGlobal();
                localStorage.setItem('g', JSON.stringify(g));
                this.setState({
                    buttonLoading: false,
                    // redirectHome: true
                })
                window.location.href = "/"
            },1000)    
        }
        else {
            setTimeout(() => {
                this.openNotification({type: "login", title: "Error", message: "Email not found, please register a new account."})
                setGlobal({
                    userIsLoggedIn: false
                })
                this.setState({
                    buttonLoading: false
                })
                this.switchFormType()
            },1000)    
        }
    }
    async register(e) {
        e.preventDefault()
        this.setState({ buttonLoading: true })
        let user = {
            email: this.email.current.value,
            password: this.password.current.value,
            strategy: "local"
        }
        console.log('[[ User Register ]]', user)
        let res = await methods.register(user)
        console.log('[[ User Register ]]',res)
        if(res.code === 500){
            setTimeout(() => {
                this.openNotification({type: "register", title: "Error", message: "User exists, please login."})
                this.setState({
                    buttonLoading: false
                })
                this.switchFormType()
            },1000)            
        }
        else {
            setTimeout(() => {
                this.openNotification({type: "register", title: "Success", message: "Registration successful, please log in."})
                this.setState({
                    buttonLoading: false
                })
                this.switchFormType()
            },1000)
        }
    }
    switchFormType() {
        this.setState({ register: !this.state.register })
    }
    componentDidMount() {
        // window.location.href = "/login";
    }
    render() {
        if(this.state.redirectHome){
            return(
                <Redirect to={"/"} />
            )
        }
        return (
            <Wrapper>
            <section id="Login" className="flex flex-column w-100 mw6 center">
                <div className="flex flex-column ">
                    <PageTitle title={this.state.register ? "Register" : "Login"} subtitle={""} />
                    <div className="flex flex-column w-100 justify-center pb4 pb0-ns">
                        <form onSubmit={ this.state.register ? this.register : this.login } className="form flex flex-column w-100 pb4 ph0">
                            <div className="form-row flex flex-column w-100 pb3">
                                <div className="form-row flex flex-column w-100">
                                    <span className="f5 fw5 black-40 pb2 ttc pb2">Email</span>
                                    <div className="flex flex-column ">
                                        <div className="flex flex-row justify-between pb3">
                                            <input type="text" ref={this.email} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1 w-100" />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="form-row flex flex-column w-100 pb3">
                                <div className="form-row flex flex-column w-100">
                                    <span className="f5 fw5 black-40 pb2 ttc pb2">Password</span>
                                    <div className="flex flex-column ">
                                        <div className="flex flex-row justify-between pb3">
                                            <input type="password" ref={this.password} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1 w-100" />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="form-button flex flex-row w-100 pt2">
                                <button type="submit" className="flex flex-column ph3 pv2 bg-black-90 br1 bs-a bn ">
                                    <span className="f5 fw5 white">{this.state.buttonLoading ? <Icon type="loading" className={'mr3 white'} /> : null } {this.state.register ? "Register" : "Login"}</span>
                                </button>
                            </div>
                            {
                                this.state.register
                                    ? <div className="form-help-text flex flex-row w-100 pt4">
                                        <span className="">Don't have an account yet? <span className="fw6 pointer" onClick={this.switchFormType}>Login</span></span>
                                    </div>
                                    : <div className="form-help-text flex flex-row w-100 pt4">
                                        <span className="">Already have an account yet? <span className="fw6 pointer" onClick={this.switchFormType}>Register</span></span>
                                    </div>
                            }

                        </form>
                    </div>
                </div>

            </section>
            </Wrapper>
        )
    }
}

export default Login