import React from 'reactn';
import {
    BrowserRouter as 
    Redirect
} from "react-router-dom";
import { Icon } from 'antd'
import PageTitle from '../../components/PageTitle';

import methods from '../../utils';
import Wrapper from '../../components/Wrapper';

class AddRecipe extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            editing: false,
            formConfig: {},
            form: {},
            activeField: null,
            activeIndex: 0,
            redirectHome: false,
            buttonLoading: false
        }
        console.log(props)

        this.insertRecipe = this.insertRecipe.bind(this);
        this.addIngredientInput = this.addIngredientInput.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.setInputValueinArray = this.setInputValueinArray.bind(this);
        this.addInput = this.addInput.bind(this);
        this.setFormConfig = this.setFormConfig.bind(this);
    }
    async insertRecipe(e) {
        e.preventDefault()
        this.setState({
            buttonLoading: true
        })
        console.log(this.state)
        let form = this.state.form;
        let data = {};
        for (var key in form) {
            if (form.hasOwnProperty(key)) {
                data[key] = form[key].value
            }
        }
        let res = await methods.insertRecipe(data);
        if (res.status) {
            this.setState({ redirectHome: true })
        }
    }
    addIngredientInput() {
        let inputs = this.state.form.ingredients.value;
        inputs.push("")
        this.setState({
            Ingredients: inputs
        })
    }
    addInput(type) {
        let form = this.state.form;
        let inputs = form[type].value;
        inputs.push("")
        form[type].value = inputs;
        this.setState({
            form: form
        })
    }
    setInputValueinArray(event) {
        const value = event.target.value;
        let form = this.state.form;
        const type = this.state.activeField;
        const index = this.state.activeIndex;
        const fields = this.state.form[type].value;
        fields[index] = value;
        form[type].value = fields;
        this.setState({
            form: form
        })
    }
    setInputValue(event) {
        const value = event.target.value;
        let form = this.state.form;
        let activeField = this.state.activeField;
        form[activeField].value = value;
        this.setState({
            form: form
        })
    }
    setFieldType(data) {
        const type = data.field.title;
        const index = data.index;
        this.setState({ activeField: type, activeIndex: index })
    }
    setFormConfig() { 
    }
    async componentDidMount() {
        let config_ = { self: this }
        return methods.initialiseForm(config_)
    }
    render() {
        if (this.state.redirectHome) {
            return (
                <Redirect to={"/"} />
            )
        }
        return (
            <Wrapper>
            <section id="AddRecipe" className="">
                <PageTitle title={this.state.editing ? "Edit Recipe" : "Add Recipe"} subtitle={this.state.editing ? "Edit the recipe using the form below" : "Add a new recipe using the form below."} />
                {
                    this.state.ready
                        ? <form className="form flex flex-column w-100 pv4 ph0">

                            {
                                this.state.formConfig.fields.map((field, index) => (

                                    field.type === "array"
                                        ? <div key={index} className="form-row flex flex-column w-100 pb4">
                                            <div className="form-row flex flex-column w-100">
                                                <span className="f5 fw5 black-40 pb2 ttc pb2">{field.label}</span>
                                                <div className="flex flex-column ">
                                                    {
                                                        field.value.map((item, index) => (
                                                            field.title === "method"
                                                                ?
                                                                <div key={index} className="flex flex-column pb3">
                                                                    <textarea defaultValue={item} onClick={() => this.setFieldType({ field, index })} type={field.title} onChange={this.setInputValueinArray} rows={2} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1" />
                                                                </div>
                                                                :
                                                                <div key={index} className="flex flex-row justify-between pb3">
                                                                    <input defaultValue={item} onClick={() => this.setFieldType({ field, index })} type={field.title} onChange={this.setInputValueinArray} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1 w-100" />

                                                                </div>
                                                        ))}
                                                </div>
                                                <label className="f6 fw1 black-40 pv2  ttc">{field.description}</label>

                                            </div>
                                            <div className="flex flex-row pt0">
                                                {
                                                    <a onClick={() => this.addInput(field.title)} className="flex flex-row ph2 pv1 bg-black-20 br1 bs-a bn ">
                                                        <span className="f6 fw5 white"><Icon type="plus" className="f7 fw6 white mr2" /> Add Step </span>
                                                    </a>
                                                }
                                            </div>
                                        </div>


                                        : (
                                            <div key={index} className="form-row flex flex-column w-100 pb4">
                                                <span className="f5 fw5 black-40 pb2 ttc pb2">{field.title}</span>
                                                <input defaultValue={field.defaultValue} onChange={this.setInputValue} onClick={() => this.setFieldType({ field, index })} ref={this[field.title]} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1" />
                                                <label className="f6 fw1 black-40 pt2 ttc">{field.label}</label>
                                            </div>
                                        )

                                ))
                            }
                            <div className="form-button flex flex-row w-100 pt4">
                                <button onClick={this.insertRecipe} className="flex flex-column ph3 pv2 bg-black-90 br1 bs-a bn ">
                                    <span className="f5 fw5 white">{this.state.buttonLoading ? <Icon type="loading" className={'mr3 white'} /> : <Icon type="plus" className="f6 fw6 white mr2" />} Add Recipe</span>
                                </button>
                            </div>
                        </form>
                        : null
                }

            </section >
            </Wrapper>
        )
    }
}

export default AddRecipe