import React from 'reactn';
import {
    BrowserRouter as
    Redirect
} from "react-router-dom";
import { notification, Popconfirm, message, Menu, Dropdown, Icon, Skeleton } from 'antd';

import methods from '../../utils';
import ui from '../../utils/ui.js';
import Wrapper from '../../components/Wrapper';


class ViewRecipe extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            recipeID: props.match.params.id,
            data: {},
            redirectHome: false
        };
        this.fetchRecipeData = this.fetchRecipeData.bind(this);
        this.image = this.image.bind(this);
        this.delete = this.delete.bind(this)
        this.edit = this.edit.bind(this)
        this.confirm = this.confirm.bind(this);
        this.cancel = this.cancel.bind(this);
        this.openNotification = this.openNotification.bind(this);

    }
    async fetchRecipeData(id) {
        let recipe = await methods.fetchRecipe(id);
        this.setState({
            data: recipe,
            ready: true
        })
    }
    openNotification = () => {
        notification.open({
            message: 'Recipe Deleted',
            description:
                'The recipe has been deleted.',
            onClick: () => {
                // console.log('Notification Clicked!');
            }
        })
    }
    image() {
        return ui.image(this.state.data)
    }
    async delete() {
        let res = await methods.deleteRecipe(this.state.recipeID)
        if (res.status) {
            this.openNotification()
            this.setState({ redirectHome: true })
        }
    }
    async edit() {
        this.setState({ redirectEdit: true })
    }
    confirm(e) {
        console.log(e);
        message.success('Recipe Deleted');
    }
    cancel(e) {
    }
    componentDidMount() {
        this.fetchRecipeData(this.state.recipeID);
    }
    render() {
        if (this.state.redirectHome) {
            return (<Redirect to={"/"} />)
        }
        if (this.state.redirectEdit) {
            window.localStorage.setItem('recipeData', JSON.stringify(this.state.data));
            window.localStorage.setItem('editRecipe', JSON.stringify(true));
            return (<Redirect to={"/edit" + "/" + this.state.recipeID} />)
        }        
        return (
            <Wrapper>
            <section id="ViewRecipe" className="flex flex-column w-100">
                {
                    this.state.ready
                        ? <div className="flex flex-column">
                            <div className="recipe-item- flex flex-column justify-between pointer">
                                <div className="flex flex-column flex-row-ns flex-auto">

                                    <div className="w-100 w-60-ns flex flex-column ph2 pr4-ns">
                                        <div className="flex flex-row flex-row-ns pb3 justify-between"><span className="f2 fw6 black-90">{this.state.data.title}</span>
                                            <div className="flex flex-column items-center justify-start ph0 ph4-ns pv3">
                                                <Dropdown overlay={<Menu>
                                                    <Menu.Item key="0">
                                                        <Popconfirm
                                                            title="Are you sure delete this recipe?"
                                                            onConfirm={this.delete}
                                                            onCancel={this.cancel}
                                                            okText="Yes"
                                                            cancelText="No"
                                                        >
                                                            <a href="#">Delete Recipe</a>
                                                        </Popconfirm>
                                                    </Menu.Item>
                                                    <Menu.Item key="1">
                                                        <Popconfirm
                                                            title="Are you sure edit this recipe?"
                                                            onConfirm={this.edit}
                                                            onCancel={this.cancel}
                                                            okText="Yes"
                                                            cancelText="No"
                                                        >
                                                            <a href="#">Edit Recipe</a>
                                                        </Popconfirm>
                                                    </Menu.Item>
                                                </Menu>} trigger={['click']}>
                                                    <a className="ant-dropdown-link pointer" href="#">
                                                        <Icon className="black-20 f4 f6-ns" type="setting" />
                                                    </a>
                                                </Dropdown>
                                            </div>
                                        </div>
                                        <div className="flex flex-row pb4 pb3-ns"><span className="f4 f7-ns fw5 black-60 ph2  ba b--black-05- bg-black-20 white br2">{this.state.data.cuisine}</span></div>
                                        <div className="flex flex-column pb4 pb3-ns"><span className="f4 fw5 black-60">{this.state.data.description}</span></div>

                                    </div>
                                    <div className="w-100 w-40-ns flex flex-column flex-auto h5 cover bg-center br1 bs-a ph2" style={{ backgroundImage: 'url(' + this.image(this.state.data) + ')' }}></div>

                                </div>
                                <div className="flex flex-column flex-row-ns flex-auto pt5 pb4">
                                    <div className="flex flex-column flex-auto ph2 w-100 w-40-m w-40-l">
                                        <div className="flex flex-column pb3">
                                            <span className="f4 fw5 black-90">Ingredients</span>
                                        </div>
                                        <div className="flex flex-column mw5">
                                            {this.state.data.ingredients.map((item, index) => (
                                                <span key={index} className="f5 fw4 black-60 pb3"><Icon type="minus" className="f7 black-20" /> {item}</span>
                                            ))}
                                        </div>
                                    </div>
                                    <div className="flex flex-column flex-auto ph2  w-100 w-60-m w-60-l">
                                        <div className="flex flex-column pb3">
                                            <span className="f4 fw5 black-90">Method</span>
                                        </div>
                                        <div className="flex flex-column">
                                            {this.state.data.method.map((item, index) => (
                                                <span key={index} className="f5 fw4 black-60 pb1 pb4"><span className="ph2 pv1 w2 h2 ba b--black-30 f4 fw6 black-90 pb1 mr1 bg-black-90 white br1">{index + 1}</span> {item}</span>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        : <Skeleton active />
                }
            </section>
            </Wrapper>
        )
    }
}

export default ViewRecipe


