import React  from "reactn";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import PrivateRoute from './utils/PrivateRoute'

import Login from './pages/Login';
import Home from './pages/Home';
import AddRecipe from './pages/AddRecipe';
import EditRecipe from './pages/EditRecipe';
import ViewRecipe from './pages/ViewRecipe';


export default function App() {
  return (
    <Router>      
      <section id="App">
        <Switch>        
              <Route path="/login" component={Login} />         
              <PrivateRoute path="/" component={Home} />
              <PrivateRoute path="/add" component={AddRecipe} />
              <PrivateRoute path="/edit/:id" component={EditRecipe} />
              <PrivateRoute path="/view/:id" component={ViewRecipe} />
        </Switch>
      </section>
    </Router>
  );
}

