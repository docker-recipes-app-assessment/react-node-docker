import React from 'reactn';
import { Select } from 'antd';

const { Option } = Select;

class Search extends React.Component {
    render() {
        return (
            <section id="Filter" className="flex flex-column w-100 w-100-ns justify-center mh1">
                <Select
                    mode="tags"
                    style={{ width: '100%' }}
                    placeholder="select ingredients to search"
                    defaultValue={[]}
                    onChange={this.props.search}
                >
                 {
                     this.props.tags.map((tag,index) => (
                        <Option value={tag}>{tag}</Option>
                     ))
                 }
                </Select>
            </section>
        )
    }
}
export default Search