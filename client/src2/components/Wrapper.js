import React from 'reactn';
import Header from './header';

class Wrapper extends React.Component {
    constructor() {
        super()
        this.state = {
           
        }

    }
  
    render() {
        
        return (
            <>
            <Header />
            <section id="Wrapper" className="sans-serif relative flex flex-column mw7 w-100 center pv4 ph4">
                

               {this.props.children}
            </section>
            </>
        )
    }
}

export default Wrapper