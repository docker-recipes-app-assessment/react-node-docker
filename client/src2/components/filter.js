import React from 'reactn';
import { Select } from 'antd';

const { Option } = Select;

class Filter extends React.Component {
    render() {
        return (
            <section id="Filter" className="flex flex-column w-100 w-100-ns justify-center mh1 mb1">
                <Select placeholder="Filter by cuisine" defaultValue={this.props.cuisines[0]} onChange={this.props.setFilter}>
                    {
                        this.props.cuisines.map((item, index) => (
                            <Option key={index} value={item}>{item}</Option>
                        ))
                    }
                    <Option value={"none"}>none</Option>
                </Select>
            </section>
        )
    }
}
export default Filter