## run
### in recipes-docker root folder run

docker-compose up


Troubleshooting:
If Docker fails to run (which it might), please try the following:
Find active docker installation name

dpkg -l | grep -i docker
Uninstall Docker
sudo apt-get purge -y docker-engine docker docker.io docker-ce 
sudo apt-get autoremove -y --purge docker-engine docker docker.io docker-ce

Then reinstall using the instructions from the Docker website. https://docs.docker.com/install/linux/docker-ce/ubuntu/

After that you might also need to install/downgrade the Containerd version, using this command.

sudo apt-get install containerd.io=1.2.6-3
